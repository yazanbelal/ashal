<?php
require '../vendor/autoload.php';

error_reporting(E_ALL);
ini_set("display_errors", 1);

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteCollectorProxy;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Medoo\Medoo;
use Twig\TwigFunction;

$container = new Container();
AppFactory::setContainer($container);

$container->set('view', function () {
    $twig = Twig::create(__DIR__ . '/../templates',
        ['cache' => __DIR__ . '/../cache']);
    $env = $twig->getEnvironment();
    $env->addFunction(new TwigFunction("asset", function ($name) {
        return '/assets/' . $name;
    }));
    $env->addFunction(new TwigFunction("img_file_exists", function($imgId){
        if (file_exists(__DIR__.'/../public/assets/images/category/'.$imgId.'.jpeg')) {
            return '/assets/images/category/'.$imgId.'.jpeg';
        }else{
            return '/assets/images/about/ab1.jpg';
        }
    }));
    return $twig;
});

$container->set('db', function () {
    $databaseUrl = parse_url(getenv("CLEARDB_DATABASE_URL"));
    return new Medoo([
        'type' => 'mysql',
        'host' => $databaseUrl['host'],
        'database' => substr($databaseUrl['path'], 1),
        'username' => $databaseUrl['user'],
        'password' => $databaseUrl['pass'],
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_general_ci',
    ]);
});

$app = AppFactory::create();

$app->add(TwigMiddleware::createFromContainer($app));

$app->get('/', function (Request $request, Response $response) {
    $categories = $this->get('db')->select("categories", [
        "id",
        "name"
    ]);
    return $this->get('view')->render($response, "home.twig", ["categories" => $categories]);
});

$app->get('/category/{category}', function (Request $request, Response $response, $args) {
    $queryParams = $request->getQueryParams();
    $items = $this->get('db')->select('items', ['id', 'name', 'papers'], ['c_id' => $queryParams['id']]);
    return $this->get('view')->render($response, "items.twig", ["items" => $items]);
});

$app->group('/admin', function (RouteCollectorProxy $group) {
    $group->get('/home', function (Request $request, Response $response) {
        $categories = $this->get('db')->select("categories", [
            "id",
            "name"
        ]);
        return $this->get('view')->render($response, '/admin/home.twig', ["categories" => $categories]);
    });

    $group->get('/items', function (Request $request, Response $response) {
        $items = $categories = $this->get('db')->select("items", [
            "id",
            "name",
            "c_id",
            "papers"
        ]);
        return $this->get('view')->render($response, '/admin/items.twig', ["items" => $items]);
    });
});

$app->get('/getPapers', function (Request $request, Response $response) {
    $queryParams = $request->getQueryParams();
    $item = $this->get('db')->get('items', ['papers', 'name'], ['id' => $queryParams['id']]);
    $papers = explode(':', $item['papers']);
    return $this->get('view')->render($response, 'papers.twig', ['name' => $item['name'], 'papers' => $papers]);
});

$app->post('/addNewCategory', function (Request $request, Response $response) {
    $postParams = $request->getParsedBody();
    $pdo = $this->get('db')->insert('categories', ['id' => $postParams['id'], 'name' => $postParams['name']]);
    if ($pdo->rowCount()) {
        $response->getBody()->write("Category Added Successfully, Redirecting in 5 Seconds <meta http-equiv='refresh' content='3; URL=/admin/home' />");
    } else {
        $response->getBody()->write("Failed to Add Category, Redirecting <meta http-equiv='refresh' content='3; URL=/admin/home' />");
    }
    return $response;
});

$app->post('/editCategory', function (Request $request, Response $response) {
    $postParams = $request->getParsedBody();
    $pdo = $this->get('db')->update('categories', ['id' => $postParams['id'], 'name' => $postParams['name']], ['id' => $postParams['old_id']]);
    if ($pdo->rowCount()) {
        $response->getBody()->write("Category Edited Successfully, Redirecting in 5 Seconds <meta http-equiv='refresh' content='3; URL=/admin/home' />");
    } else {
        $response->getBody()->write("Failed to Edit Category, Redirecting <meta http-equiv='refresh' content='3; URL=/admin/home' />");
    }
    return $response;
});

$app->post('/deleteCategory', function (Request $request, Response $response) {
    $postParams = $request->getParsedBody();
    if ($postParams['id']) {
        $pdo = $this->get('db')->delete('categories', ['id' => $postParams['id']]);
        if ($pdo->rowCount()) {
            $response->getBody()->write("Category Deleted, Redirecting in 10 Seconds <meta http-equiv='refresh' content='5; URL=/admin/home' />");
        } else {
            $response->getBody()->write("Failed to Delete Category, Redirecting <meta http-equiv='refresh' content='5; URL=/admin/home' />");
        }
    }else{
        $response->getBody()->write("Error: ID Missing");
    }
    return $response;
});

$app->post('/addNewItem', function (Request $request, Response $response) {
    $postParams = $request->getParsedBody();
    $pdo = $this->get('db')->insert('items', ['id' => $postParams['id'], 'name' => $postParams['name'], 'c_id' => $postParams['c_id'], 'papers' => $postParams['papers']]);
    if ($pdo->rowCount()) {
        $response->getBody()->write("Item Added Successfully, Redirecting in 5 Seconds <meta http-equiv='refresh' content='3; URL=/admin/items' />");
    } else {
        $response->getBody()->write("Failed to Add Item, Redirecting <meta http-equiv='refresh' content='3; URL=/admin/items' />");
    }
    return $response;
});

$app->post('/editItem', function (Request $request, Response $response) {
    $postParams = $request->getParsedBody();
    $pdo = $this->get('db')->update('items', ['id' => $postParams['id'], 'name' => $postParams['name'], 'c_id' => $postParams['c_id'], 'papers' => $postParams['papers']], ['id' => $postParams['old_id']]);
    if ($pdo->rowCount()) {
        $response->getBody()->write("Item Edited Successfully, Redirecting in 5 Seconds <meta http-equiv='refresh' content='3; URL=/admin/items' />");
    } else {
        $response->getBody()->write("Failed to Edit Item, Redirecting <meta http-equiv='refresh' content='3; URL=/admin/items' />");
    }
    return $response;
});

$app->post('/deleteItem', function (Request $request, Response $response) {
    $postParams = $request->getParsedBody();
    if ($postParams['id']) {
        $pdo = $this->get('db')->delete('items', ['id' => $postParams['id']]);
        if ($pdo->rowCount()) {
            $response->getBody()->write("Item Deleted, Redirecting in 10 Seconds <meta http-equiv='refresh' content='5; URL=/admin/items' />");
        } else {
            $response->getBody()->write("Failed to Delete Item, Redirecting <meta http-equiv='refresh' content='5; URL=/admin/items' />");
        }
    }else{
        $response->getBody()->write("Error: ID Missing");
    }
    return $response;
});

$app->run();